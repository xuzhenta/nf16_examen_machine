#include <stdio.h>
#include "Pile.h"
#include "File.h"
#include "ABR.h"
int main() {
    Pile p;
    p=Creer_Pile();
    Empiler(&p,2);
    Empiler(&p,10);
    Empiler(&p,102);
    Empiler(&p,11);
    Depiler(&p);
    afficher_p(p);
    echanger(&p);
    afficher_p(p);

    File f;
    f=Creer_File();
    Enfiler(&f,20);
    Enfiler(&f,10);
    Enfiler(&f,15);
    Defiler(&f);
    afficher_f(f);
    inverser(&f);
    afficher_f(f);

    Abr *a=Creer_ABR(10);
    insertion(a,10);
    insertion(a,20);
    insertion(a,12);
    insertion(a,6);
    insertion(a,8);
    insertion(a,7);
    afficher(*a);
    printf("%d\n",nombre_feuilles(*a));
    printf("%d\n",nombre_neouds_internes(*a));
    printf("%d\n",hauteur(*a));
    printf("est %d",est_equilibre(*a));
    printf("%d\n", recherche(a,8)->cle);
    supprimer(a,10);
    afficher(*a);
    return 0;
}
