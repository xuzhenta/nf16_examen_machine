//
// Created by lenovo on 2022/12/10.
//

#ifndef EXAMEN_MACHINE_PILE_H
#define EXAMEN_MACHINE_PILE_H
#include <stdio.h>
#include <stdlib.h>
#define MAX_P 20
typedef struct Pile{
    int sommet;
    int tab[MAX_P];
}Pile;

Pile Creer_Pile();
int Pile_vide(Pile * p);
int Pile_pleine(Pile * p);
void Empiler(Pile * p, int x);
int Depiler(Pile * p);
void afficher_p(Pile p);
void echanger(Pile * p);
#endif //EXAMEN_MACHINE_PILE_H
