//
// Created by lenovo on 2022/12/10.
//

#include "File.h"
File Creer_File(){
    File f;
    f.tete=0;
    f.queue=0;
    return f;
}

int File_vide(File *f){
    return (f->tete==f->queue);
}

int File_pleine(File *f){
    int i=(f->queue+1)%MAX_F;
    return (i==f->tete);
}

void Enfiler(File *f,int valeur){
    if(File_pleine(f)){
        printf("File est pleine\n");
        return ;
    }
    else {
        f->tab[f->queue]=valeur;
        f->queue=(f->queue+1)%MAX_F;
    }
}

int Defiler(File *f){ //File FIFO , donc supprimer le tete
    if(File_vide(f)){
        printf("File est vide\n");
        return 0;
    } else{
        int val=f->tab[f->tete];
        f->tete=(f->tete+1)%MAX_F;
        return val;
    }
}

void afficher_f(File f){
    if(File_vide(&f))
        return;
    else{
        int i=f.tete;
        while (i!=f.queue){
            printf("%d ",f.tab[i]);
            i=(i+1)%MAX_F;
        }
    }
}

void inverser(File *f){
    int val[MAX_F];
    int i=0;
    while (!File_vide(f)){
        val[i]= Defiler(f);
        i++;
    }
    for(int j=i-1;j>=0;j--){
        Enfiler(f,val[j]);
    }
}