//
// Created by lenovo on 2022/12/11.
//

#include "ABR.h"
Abr * Creer_ABR(int val){
    Abr * a=malloc(sizeof(Abr));
    a->gauche=NULL;
    a->droit=NULL;
    a->cle=val;
    return a;
}

void insertion(Abr *a,int cle){
    if(a!=NULL) {
        Abr *y = a;
        Abr *x;
        while (y != NULL) {
            x=y;
            if (cle <= y->cle)
                y = y->gauche;
            else
                y = y->droit;
        }
        if(cle<=x->cle)
            x->gauche= Creer_ABR(cle);
        else
            x->droit= Creer_ABR(cle);
    }
    else
        a=Creer_ABR(cle);
}

void afficher(Abr a){
    Abr * x=&a;
    if (x!=NULL){
        if(x->gauche!=NULL) {
            afficher(*(x->gauche));
        }
        printf("%d ",x->cle);
        if(x->droit!=NULL) {
            afficher(*(x->droit));
        }
    }
}

int nombre_feuilles(Abr a){
    Abr * x=&a;
    if(x==NULL)
        return 0;
    if(x->gauche==NULL&&x->droit==NULL)
        return 1;
    if(x->gauche!=NULL&&x->droit!=NULL)
        return (nombre_feuilles(*(x->gauche))+nombre_feuilles(*(x->droit)));
    else if(x->gauche!=NULL&&x->droit==NULL)
        return  (nombre_feuilles(*(x->gauche)));
    else if(x->gauche==NULL&&x->droit!=NULL)
        return  (nombre_feuilles(*(x->droit)));
}

int nombre_neouds_internes(Abr a){
    Abr * x=&a;
    if(x==NULL)
        return 0;
    if(x->gauche!=NULL&&x->droit!=NULL)
        return 1+(nombre_neouds_internes(*(x->gauche))+nombre_neouds_internes(*(x->droit)));
    else if(x->gauche!=NULL&&x->droit==NULL)
        return  1+(nombre_neouds_internes(*(x->gauche)));
    else if(x->gauche==NULL&&x->droit!=NULL)
        return  1+(nombre_neouds_internes(*(x->droit)));
}

int hauteur(Abr a){
    Abr *x=&a;
    int m=0,n=0;
    if(x==NULL)
        return 0;
    if(x->gauche)
        m= hauteur(*(x->gauche));
    if(x->droit)
        n= hauteur(*(x->droit));
    if(m>=n){
        return 1+m;
    }
    else
        return 1+n;
}

int est_equilibre(Abr a){
    int m=0,n=0;
    if(a.gauche)
        m= hauteur(*(a.gauche));
    if(a.droit)
        n= hauteur(*(a.droit));
    if(abs(m-n)<=1)
        return 1;
    else
        return 0;
}

Abr * recherche(Abr *a,int cle){
    if(a==NULL)
        return NULL;
    Abr *x=a;
    while (x!=NULL && x->cle!=cle){
        if(cle<=x->cle)
            x=x->gauche;
        else
            x=x->droit;
    }
    return x;
}

Abr * supprimer(Abr *a,int cle){
    if(a==NULL)
        return NULL;
    else if(cle<a->cle)
        a->gauche= supprimer(a->gauche,cle);
    else if(cle>a->cle)
        a->droit= supprimer(a->droit,cle);
    else{
        if(a->droit==NULL||a->gauche==NULL){  //若仅有一个子节点，直接设置为该子节点
            Abr *b =(a->droit)?a->droit:a->gauche;
            return b;
        }
        else{ //若有两个子节点，交换其与succ的值，后删除succ
            Abr *b=a->droit;
            while (b->gauche!=NULL)
                b=b->gauche;
            a->cle=b->cle;
            a->droit= supprimer(a->droit,a->cle);
        }
    }
    return a;
}