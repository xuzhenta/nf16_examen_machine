#include "Pile.h"
// pour le type Pile , le sommet est le position du dérnier élément
Pile Creer_Pile(){
    Pile p;
    p.sommet=-1; //tableau commencer par 0
    return p;
}

int Pile_vide(Pile * p){
    if(p->sommet==-1){
        return 1;
    }
    return 0;
}

int Pile_pleine(Pile * p){
    if(p->sommet==MAX_P-1){
        return 1;
    }
    return 0;
}

void Empiler(Pile * p, int x){
    if(!Pile_pleine(p)) {
        p->sommet++;
        p->tab[p->sommet] = x;
    }
    else
        printf("Pile est pleine\n");
}

int Depiler(Pile * p){
    if(!Pile_vide(p)){
        p->sommet--;
        return p->tab[p->sommet+1];
    }
    else
        printf("Pile est vide\n");
}

void afficher_p(Pile p){
    for(int i=0;i<=p.sommet;i++){
        printf("%d ",p.tab[i]);
    }
}

void echanger(Pile * p){
    int temp;
    temp=p->tab[0];
    p->tab[0]=p->tab[p->sommet];
    p->tab[p->sommet]=temp;
}
