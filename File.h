//
// Created by lenovo on 2022/12/10.
//

#ifndef EXAMEN_MACHINE_FILE_H
#define EXAMEN_MACHINE_FILE_H
#include <stdio.h>
#define MAX_F 20
typedef struct File{
    int tete;
    int queue;
    int tab[MAX_F];
}File;
File Creer_File();
int File_vide(File * f);
int File_pleine(File *f);
void Enfiler(File *f,int valeur);
int Defiler(File *f);
void afficher_f(File f);
void inverser(File *f);
#endif //EXAMEN_MACHINE_FILE_H
