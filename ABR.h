
//
// Created by lenovo on 2022/12/11.
//

#ifndef EXAMEN_MACHINE_ABR_H
#define EXAMEN_MACHINE_ABR_H
#include <stdio.h>
#include <stdlib.h>
typedef struct ABR{
    struct ABR * gauche;
    struct ABR * droit;
    int cle;
}Abr;

Abr * Creer_ABR(int val);
void insertion(Abr *a,int cle);
void afficher(Abr a);
int nombre_feuilles(Abr a);
int nombre_neouds_internes(Abr a);
int hauteur(Abr a);
int est_equilibre(Abr a);
Abr * recherche(Abr *a,int cle);
Abr *supprimer(Abr *a,int cle);
#endif //EXAMEN_MACHINE_ABR_H
